import React from 'react';
import XPiece from './XPiece.js';
import OPiece from './OPiece.js';
import BlankPiece from './BlankPiece.js';

class Board extends React.Component {

   renderSquare = {
            r1c1: <OPiece />,
            r1c2: <BlankPiece />,
            r1c3: <XPiece />,
            r2c1: <BlankPiece />,
            r2c2: <OPiece />,
            r2c3: <BlankPiece />,
            r3c1: <XPiece />,
            r3c2: <BlankPiece />,
            r3c3: <XPiece />
        };                  
             
    render() {    
    return (
      <div>
        <div className="board-row">
          {this.renderSquare.r1c1}
          {this.renderSquare.r1c2}
          {this.renderSquare.r1c3}
        </div>
         <div className="board-row">
          {this.renderSquare.r2c1}
          {this.renderSquare.r2c2}
          {this.renderSquare.r2c3}
        </div>
        <div className="board-row">
          {this.renderSquare.r3c1}
          {this.renderSquare.r3c2}
          {this.renderSquare.r3c3}
        </div>
      </div>
    );
  }
}

export default Board;