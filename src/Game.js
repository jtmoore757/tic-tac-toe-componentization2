import React, { Component } from 'react';
import logo from './logo.svg';
import './Game.css';
import Board from './board1.js';

class Game extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="Game-title">Welcome Jim</h1>
        </header>
        <p className="Game-intro">
          To get started, edit <code>src/Game.js</code> and save to reload.
        </p>
        
        <div className="game-board">
          <Board />

        </div>

      </div>
      
     
    );
  }
}

export default Game;
