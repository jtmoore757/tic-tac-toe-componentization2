import React from 'react';
import Square from './Square.js';

class XPiece extends React.Component {
    
  render() {
      return <Square value='X' />;
    }
  }

export default XPiece;