import React from 'react';
import xPiece from './XPiece.js';

class Square extends React.Component {
    
  render() {
      return (
        <button className="square">
        {this.props.value('X')}
        {/* <button className="square" onClick={() => this.setState({value: 'X'})}> */}
        {/* {this.state.value} */}
        </button>
      );
    }
  }

class Board extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      squares: Array(9).fill(null),
    };
  }

  // xMoves(i) {
  //   const squares = this.state.squares.slice();
  //   squares[i] = xPiece;
  //   this.setState({squares: squares});
  // }

  renderSquare(i) {
    // return <Square 
    //   value={this.state.squares[i]} />;

    // const dispX = [0, 2, 6, 8];
    // const dispO = [1, 4, 5,];

      switch(i) {
        case i=0:
          return <Square value={<xPiece />} />
          // return (
          //   <Square 
          //     value={this.state.squares[i]} 
          //     />
          // );
        case i=4:
          return <Square value='O' />;
        default:
          return <Square value='' />;
      }
   
     }

  render() {    
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

export default Board;