import React from 'react';
import Square from './Square.js';

class OPiece extends React.Component {
    
  render() {
      return <Square value='O' />;
    }
  }

export default OPiece;