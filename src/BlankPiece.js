import React from 'react';
// import Square from './Square.js';

class Blank extends React.Component {
    
    render() {
        return (
          <button className="blank">
            {this.props.value}
          </button>
        );
      }
    }
  

class BlankPiece extends React.Component {
  
    render() {
        return <Blank value='!' />;
      }
    }


  
export default BlankPiece;